// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/a/RAM8.hdl

/**
 * Memory of 8 registers, each 16 bit-wide. Out holds the value
 * stored at the memory location specified by address. If load==1, then 
 * the in value is loaded into the memory location specified by address 
 * (the loaded value will be emitted to out from the next time step onward).
 */

CHIP RAM8 {
    IN in[16], load, address[3];
    OUT out[16];

    PARTS:
    DMux8Way(in=load,sel=address,a=load00,b=load01,c=load02,d=load03,e=load04,f=load05,g=load06,h=load07);
	Register(in=in,load=load00,out=Reg00);
	Register(in=in,load=load01,out=Reg01);
	Register(in=in,load=load02,out=Reg02);
	Register(in=in,load=load03,out=Reg03);
	Register(in=in,load=load04,out=Reg04);
	Register(in=in,load=load05,out=Reg05);
	Register(in=in,load=load06,out=Reg06);
	Register(in=in,load=load07,out=Reg07);
	Mux8Way16(a=Reg00,b=Reg01,c=Reg02,d=Reg03,e=Reg04,f=Reg05,g=Reg06,h=Reg07,sel=address,out=out);
}